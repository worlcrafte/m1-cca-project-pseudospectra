# Pseudospectra Computation Interface

This project includes a user-friendly interface for computing pseudospectra using two algorithms: Grid and Curve Tracing. The interface allows users to manipulate the implemented algorithms without any prior knowledge of MATLAB.

## Features

- **Algorithm Selection and Execution**:

  - Compute pseudospectra using both Grid and Curve Tracing algorithms.
  - Select either a random matrix or load your own matrix for computation.
  - Option to display results of both algorithms or just one for easy comparison.

- **Performance Measurement**:

  - Measure the performance of the algorithms.
  - Display either the efficiency or speedup of the algorithms using a switch button.

- **Graph Redrawing and Zooming**:

  - Supports redrawing the graph for zooming purposes when using the Grid algorithm.
  - Helps in detailed analysis and visualization of specific regions of the computed pseudospectra.

- **Default Settings**:
  - All data displayed at the start of the application is set to default base values.
  - If the user does not change these settings, the pseudospectra will be computed using these default values.

## Getting Started

## Dependencies

- MATLAB.
- The addons provided by MATLAB (Statistics and machine learning addon, parralel computing toolbox...).

### Launching the Application

1. Open the application in MATLAB.
2. The main interface window will appear with options to select algorithms, input matrices, and configure settings.

### Inputting Data

- **Random Matrix**: Select the option to use a random matrix for computation.
- **Loaded Matrix**: Use the option to load a matrix from your local files.

### Configuring Algorithm Parameters

- Set the desired parameters for the Grid and Curve Tracing algorithms.
- Parameters include tolerance levels, step lengths, and other relevant settings.

### Running the Algorithms

1. Click the "Start" button to run the selected algorithm(s) with the configured settings.
2. The application will display the computed pseudospectra on the graph.

### Visualizing Results

- Use the provided controls to zoom in on specific regions of the graph.
- Switch between efficiency and speedup displays to analyze performance.

### Adjusting and Rerunning

- Modify the parameters or switch algorithms as needed.
- Rerun the computations to see updated results and performance metrics.

## Additional Information

This interface aims to simplify the process of computing and analyzing pseudospectra, making it accessible even to users without a background in MATLAB. For detailed instructions and additional information, refer to the appendix of the report on the project.

The progressbar is not written by us. You can find the source code here : https://fr.mathworks.com/matlabcentral/fileexchange/6922-progressbar 

## Acknowledgements

- Testing was performed on several random and known matrices to ensure the correctness of the computed pseudospectra.

## Know issue
- If you have "Unreconized function or varaible 'udp'", this is due to the probress bar, you will nedd to delete it in grid and curve tracing file and delete the load for progressbar. 
- If it's parpool lauching problem you will need to restart the app.

## Authors@

- **FLANDIN Leo** - [worlcrafte](https://github.com/worlcrafte)
- **GOGRITCHIANI Lasha** - [LashaGOG](https://github.com/LashaGOG)
